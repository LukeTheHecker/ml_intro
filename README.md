# Introduction to machine learning with Keras & Tensorflow in Python 3


This is the repository in which you'll find the scripts and notes of the machine learning introduction by me (Lukas) showing basic functionalities of Keras and Tensorflow. 

Most of the code is copied from the world wide web, including:
https://keras.io/examples/mnist_cnn/

## Setup

First to set up your coding environment you have to install the following software.

### Repository

Go to my Bitbucket and download or clone the repository which contains all the files used in this tutorial.

https://bitbucket.org/LukeTheHecker/ml_intro/src/master/

### Anaconda

Anaconda is a software that most importantly enables you to create **environments**. A environment can be viewed as a container that contains the specific python version and all the packages you need for a given project. It is very practical for code management if for different projects you need different versions of open-source libraries. I created an Anaconda environment for the scope of this course so we have all the same necessary libraries for performing machine learning. I could create an environment that is set up to train networks on a *GPU* and another one to train them on a *CPU*.

Download Anaconda here:

https://www.anaconda.com/distribution/

Select your operating system and make sure you choose **python 3.7**.

After installing Anaconda you have to open the Anaconda prompt and create the environment. For this, open the Anaconda prompt (a Terminal) and change to the working directory. For me this was:

```
cd C:\Users\Lukas\Documents\ml_intro
```

In this directory you should find the file 

```
environment_basic-ml.yml
```

This .yml file is basically just a list of all the necessary libraries which you will need. You can now easily create your environment based on this .yml file by typing

```
conda env create --name ml_intro --file environment.yml
```

and follow the instructions in the prompt. Then you should activate your environment and open jupyter lab by typing

```
conda activate ml_intro
jupyter lab
```

This should open jupyter lab in your standard browser. Here you change to your working directory and open e.g. mnist_example.ipynb

In this file you right click and select *New console for Notebook* and enter these commands into this console successively:

```
conda activate ml_intro
conda install ipykernel
ipython kernel install --user --name=ml_intro_kernel
conda deactivate
```

Then you restart your kernel and open mnist_example.ipynb again, click on the kernel button on the top right and select the *ml_intro_kernel* which we just created in the last four lines of code.

## Finally...

...you should now be able to follow along the jupyter notebooks covered in this tutorial - have fun :)

Now that everything is set up you can start up your jupyter notebooks as follows:

```
# Open anaconda prompt
conda activate ml_intro
jupyter lab
# right click into your jupyter notebook
# select *new console for notebook* 
```