# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 23:19:17 2019

@author: Lukas
"""

from __future__ import print_function
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.keras import backend as K
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from lh_utils import get_kernel, shift_kernel, norm_me, add_noise
%matplotlib qt

nsamples_tr = int(1e4)
nsamples_tst = int(1e3)
batch_size = 32
epochs = 50
input_shape = (32, 32, 1)



# Create Some Kernel Data
noiselvl = (0.05, 0.25)

x = np.zeros(shape=(nsamples, input_shape[0], input_shape[0]))
y = np.zeros(shape=(nsamples, input_shape[0], input_shape[0]))

for i in range(nsamples):
    if i % 1000 == 0 or i==nsamples-1:
        print("{} samples".format(i))
    y_tmp = get_kernel()
    y_shifted = shift_kernel(y_tmp)
    y[i,] = norm_me(y_shifted)
    x[i,] = add_noise(y[i,], noiselvl)

# Reshape to have empty color dimension (grey scales)
x = np.expand_dims(x, axis=3)
y = np.expand_dims(y, axis=3)

# Split Data into Training, Validation and Test Set (80 - 10 - 10 %)
x_tr = x[0:int(nsamples*0.8), ]
y_tr = y[0:int(nsamples*0.8), ]

x_val = x[int(nsamples*0.8):int(nsamples*0.9), ]
y_val = y[int(nsamples*0.8):int(nsamples*0.9), ]

x_tst = x[int(nsamples*0.9):, ]
y_tst = y[int(nsamples*0.9):, ]



# Build network Architecture using the functional API:

input_img = Input(shape=input_shape)  # adapt this if using `channels_first` image data format

x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(input_img)
x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)

x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)
decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same', data_format='channels_last')(x)

autoencoder = Model(input_img, decoded)

autoencoder.summary()

optimizer = tf.keras.optimizers.SGD(lr=0.01, momentum=0.9)

autoencoder.compile(optimizer=optimizer, loss='mean_squared_error')

autoencoder.fit(x_tr, y_tr,
                epochs=epochs,
                batch_size=batch_size,
                shuffle=True,
                validation_data=(x_test, y_test),
                #callbacks=[TensorBoard(log_dir='/tmp/tb', histogram_freq=0, write_graph=False)]
                )

# Create Some Kernel Data


p = autoencoder.predict(x_test)


sample = np.random.randint(low=0, high=nsamples_tst)

f, (ax1, ax2, ax3) = plt.subplots(1, 3)

im = ax1.imshow(np.squeeze(y_test[sample,]))
# f.colorbar(im, ax=ax1)
ax1.set_title("Ground Truth")

im = ax2.imshow(np.squeeze(x_test[sample,]))
# f.colorbar(im, ax=ax2)
ax2.set_title("Noisy input to ANN")


im = ax3.imshow(np.squeeze(p[sample,]))
# f.colorbar(im, ax=ax3)
ax3.set_title("Denoised Prediction")

