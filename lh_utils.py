# -*- coding: utf-8 -*-
"""
Some useful functions for the introduction to machine learning workshop
Created on Tue Nov 19 23:38:31 2019

@author: Lukas
"""
import scipy
from scipy import stats
from scipy.stats import skewnorm
import numpy as np


def get_kernel():
    x = stats.norm.rvs(scale=0.5, size=20)
    y = stats.norm.rvs(scale=0.5, size=20)
    data = np.vstack([x+y, x-y])

    mydensity = stats.gaussian_kde(data)

    X, Y = np.mgrid[-4:4:32j, -4:4:32j]
    grid = np.vstack([X.ravel(), Y.ravel()])
    Z = mydensity(grid)
    Z = np.reshape(Z, X.shape)

    return Z



def shift_kernel(x, low = -10, high = 10):
    x_shift = np.random.randint(low=low, high=high)
    y_shift = np.random.randint(low=low, high=high)
    
    return scipy.ndimage.shift(x, (x_shift,y_shift))


def norm_me(x):
    return x / np.max(np.abs(x))

def add_noise(x, noiselvl=(0.01, 0.1)):
    
    # random noise in range of -1 to 1:
    noise = 2 * (np.random.rand(x.shape[0], x.shape[0]) - 0.5)
    lvl = np.random.uniform(low=noiselvl[0], high=noiselvl[1], size=(1,))
    x = x + (noise * lvl)
    return x / np.max(np.abs(x))




