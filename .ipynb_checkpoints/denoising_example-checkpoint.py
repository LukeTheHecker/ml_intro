# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 23:19:17 2019

@author: Lukas
"""

from __future__ import print_function
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.keras import backend as K
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from lh_utils import get_kernel, shift_kernel, norm_me, add_noise
%matplotlib qt

nsamples_tr = int(1e4)
nsamples_tst = int(1e3)
batch_size = 32
epochs = 50
input_shape = (32, 32, 1)



# Create Some Kernel Data
noiselvl = (0.05, 0.25)

x_tr = np.zeros(shape=(nsamples_tr, input_shape[0], input_shape[0]))
y_tr = np.zeros(shape=(nsamples_tr, input_shape[0], input_shape[0]))

for i in range(nsamples_tr):
    if i % 100 == 0:
        print("{} samples".format(i))
    y_tmp = get_kernel()
    y_shifted = shift_kernel(y_tmp)
    y_tr[i,] = norm_me(y_shifted)
    x_tr[i,] = add_noise(y_tr[i,], noiselvl)

'''
sample = np.random.randint(low=0, high=nsamples_tr)
plt.imshow(np.squeeze(x_tr[sample,]))
plt.colorbar()

plt.imshow(np.squeeze(y_tr[sample,]))
plt.colorbar()
'''
# Reshape to have empty color dimension (grey scales)
x_tr = np.expand_dims(x_tr, axis=3)
y_tr = np.expand_dims(y_tr, axis=3)


x_test = np.zeros(shape=(nsamples_tst, input_shape[0], input_shape[0]))
y_test = np.zeros(shape=(nsamples_tst, input_shape[0], input_shape[0]))

for i in range(nsamples_tst):
    if i % 100 == 0:
        print("{} samples".format(i))
    y_tmp = get_kernel()
    y_shifted = shift_kernel(y_tmp)
    y_test[i,] = norm_me(y_shifted)
    x_test[i,] = add_noise(y_test[i,], noiselvl)

# Reshape to have empty color dimension (grey scales)
x_test = np.expand_dims(x_test, axis=3)
y_test = np.expand_dims(y_test, axis=3)


# Build autoencoder:
input_img = Input(shape=input_shape)  # adapt this if using `channels_first` image data format

x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(input_img)
x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)

x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format='channels_last')(x)
decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same', data_format='channels_last')(x)

autoencoder = Model(input_img, decoded)

autoencoder.summary()

optimizer = tf.keras.optimizers.SGD(lr=0.01, momentum=0.9)

autoencoder.compile(optimizer=optimizer, loss='mean_squared_error')

autoencoder.fit(x_tr, y_tr,
                epochs=epochs,
                batch_size=batch_size,
                shuffle=True,
                validation_data=(x_test, y_test),
                #callbacks=[TensorBoard(log_dir='/tmp/tb', histogram_freq=0, write_graph=False)]
                )

# Create Some Kernel Data


p = autoencoder.predict(x_test)


sample = np.random.randint(low=0, high=nsamples_tst)

f, (ax1, ax2, ax3) = plt.subplots(1, 3)

im = ax1.imshow(np.squeeze(y_test[sample,]))
# f.colorbar(im, ax=ax1)
ax1.set_title("Ground Truth")

im = ax2.imshow(np.squeeze(x_test[sample,]))
# f.colorbar(im, ax=ax2)
ax2.set_title("Noisy input to ANN")


im = ax3.imshow(np.squeeze(p[sample,]))
# f.colorbar(im, ax=ax3)
ax3.set_title("Denoised Prediction")

